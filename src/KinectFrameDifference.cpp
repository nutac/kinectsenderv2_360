#include "KinectFrameDifference.h"
#define RECT_SPEED 5

KinectFrameDifference::KinectFrameDifference() {
}


void KinectFrameDifference::setup(int width, int height, string ndiSenderName) {
	w = width;
	h = height;

	processedPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
	previousPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
	differencePixels.allocate(width, height, OF_IMAGE_GRAYSCALE);

	processedTexture.allocate(processedPixels);
	processedTexture2.allocate(previousPixels);


	senderName = ndiSenderName;
	fbo.allocate(width, height, GL_RGBA);
	ndiSender.SetReadback();
	ndiSender.SetAsync();
	ndiSender.CreateSender(senderName.c_str(), width, height);

	//createRectangles(10, 50);

}
void KinectFrameDifference::createRectangles(int amount, int width, int top, int height) {
	if (!isPianoMode) {
		createZones(amount, width, top, height);
		return;
	}

	rectsAmount = amount;
	rectsWidth = width;
	rectsTop = top;
	rectsHeight = height;

	rectangles.clear();
	rectAlpha.clear();
	rectTriggered.clear();
	int rectWidth = w / rectsAmount;

	for (int i = 0; i < rectsAmount; i++) {
		ofRectangle rect;
		rect.x = i * rectWidth;
		rect.y = h * float((float(rectsTop) / 100));;
		rect.width = rectWidth * float((float(rectsWidth) / 100));
		//rect.height = h*0.5;
		rect.height = h * float((float(rectsHeight) / 100));


		rectangles.push_back(rect);
		rectAlpha.push_back(rectAlphaMin);
		rectTriggered.push_back(false);

	}
}
void KinectFrameDifference::createZones(int amount, int width, int top, int height) {
	rectsAmount = amount;
	rectsWidth = width;
	rectsTop = top;
	rectsHeight = height;

	cout << amount << ", " << width << endl;
	rectangles.clear();
	rectAlpha.clear();
	rectTriggered.clear();
	rectVelocities.clear();

	int rectWidth = w * 0.5;
	int maxAttempts = 100;

	for (int i = 0; i < amount; i++) {
		bool isOverlapping = false;
		int attempts = 0;

		ofRectangle newRect;

		do {
			//float w= rectWidth * float((float(width) / 100));
			float wi = rectWidth * float((float(rectsWidth) / 100));
			int x = ofRandom(0, w - wi);
			int y = ofRandom(0, h - wi);
			newRect.set(x, y, wi, wi);

			isOverlapping = false;

			if (i == 0) {
				if (attempts == 0) {
					//cout << amount << ", " << width << " - " << wi << endl;

				}
			}


			for (const auto& existingRect : rectangles) {
				if (newRect.intersects(existingRect)) {
					isOverlapping = true;
					break;
				}
			}

			attempts++;

		} while (isOverlapping && attempts < maxAttempts);

		if (!isOverlapping) {
			rectangles.push_back(newRect);
			//cout << i << ": " << newRect.x << ", " << newRect.y << " - " << newRect.width << endl;
			rectAlpha.push_back(rectAlphaMin);
			rectTriggered.push_back(false);
		}

		ofVec2f velocity(ofRandom(-RECT_SPEED, RECT_SPEED), ofRandom(-RECT_SPEED, RECT_SPEED));
		rectVelocities.push_back(velocity);
	}
	//cout << rectangles.size() << endl;
}
void KinectFrameDifference::updateRectsPosition() {
	if (!isPianoMode) {
		for (int i = 0; i < rectangles.size(); i++) {
			// Actualiza la posici�n del rect�ngulo
			rectangles[i].x += rectVelocities[i].x;
			rectangles[i].y += rectVelocities[i].y;

			// Verifica si el rect�ngulo ha chocado con los bordes y ajusta su velocidad
			if (rectangles[i].x <= 0 || rectangles[i].x + rectangles[i].width >= w) {
				rectVelocities[i].x = -rectVelocities[i].x;
			}
			if (rectangles[i].y <= 0 || rectangles[i].y + rectangles[i].height >= h) {
				rectVelocities[i].y = -rectVelocities[i].y;
			}
		}
	}
}

void KinectFrameDifference::update(const ofPixels& depthPixels, bool showDifferencePixels, const int& alphaDecay, const float& minPixelsToTrigger, int _rectAlphaMin, int midiStep, int _sensibilidad) {
	if (!depthPixels.isAllocated()) return;

	sensibilidad = _sensibilidad;

	int height = h;
	int width = w;

	int step = 2;
	rectAlphaMin = _rectAlphaMin;

	//const unsigned short DIFF_THRESHOLD = 10;
	const unsigned short DIFF_THRESHOLD = sensibilidad;

	differencePixels.set(0);

	for (int y = 0; y < height - step; y += step) {
		for (int x = 0; x < width - step; x += step) {
			unsigned char currentBrightness = depthPixels.getColor(x, y).getBrightness();
			unsigned char prevBrightness = previousPixels.getColor(x, y).getBrightness();

			if (abs(currentBrightness - prevBrightness) > DIFF_THRESHOLD) {
				differencePixels.setColor(x, y, ofColor(255));
			}
		}
	}

	for (int i = 0; i < rectAlpha.size(); i++) {
		rectAlpha[i] = rectAlpha[i] - alphaDecay;
		if (rectAlpha[i] <= rectAlphaMin) {
			rectAlpha[i] = rectAlphaMin;
			if (rectTriggered[i] == true) {
				rectTriggered[i] = false;
				triggeredNotesOff.push_back(i * midiStep);
			}
		}
	}

	if (firstBoot <= 0) {
		updateRectsPosition();

		/// TRIGGER
		for (int i = 0; i < rectangles.size(); i++) {
			if (isRectTriggered(rectangles[i], rectAlpha[i], differencePixels, minPixelsToTrigger)) {
				rectAlpha[i] = 255;
				rectTriggered[i] = true;
				int pitch = i * midiStep;
				triggeredNotesOn.push_back(pitch);
			}
		}
	}
	else {
		firstBoot--;
	}

	previousPixels = depthPixels;
	processedTexture.loadData(differencePixels);

	fbo.begin();
	ofClear(0, 0, 0, 255);

	if (showDifferencePixels) {
		processedTexture.draw(0, 0);
	}

	for (int i = 0; i < rectangles.size(); i++) {
		ofSetColor(255, rectAlpha[i]);
		if (isPianoMode) {
			ofDrawRectangle(rectangles[i]);
		}
		else {
			float ww = rectangles[i].width;
			float rad = ww * 0.5;
			float xx = rectangles[i].x + rad;
			float yy = rectangles[i].y + rad;

			ofDrawCircle(xx, yy, rad);
		}
	}

	fbo.end();

}

void KinectFrameDifference::draw(ofRectangle targetRectangle) {
	if (!fbo.isAllocated()) return;

	fbo.draw(targetRectangle);
}

void KinectFrameDifference::send() {
	if (!fbo.isAllocated()) return;

	ndiSender.SendImage(fbo);
}

bool KinectFrameDifference::isRectTriggered(const ofRectangle& rect, const int& alpha, const ofPixels& differenceImage, const float& minChangedPixels) {
	int count = 0;
	bool res = false;
	int totalPixels = rect.getArea();
	int minPixels = totalPixels * float((float(minChangedPixels) / 100));
	if (alpha <= rectAlphaMin) {
		for (int y = rect.y; y < rect.y + rect.height; y++) {
			for (int x = rect.x; x < rect.x + rect.width; x++) {
				if (differenceImage.getColor(x, y).getBrightness() > 0) {
					count++;
					if (count > minPixels) {
						res = true;
						break;
					}
				}
			}
			if (res == true) break;
		}
	}
	return res;
}
std::vector<int> KinectFrameDifference::getTriggeredNotesOn() {
	std::vector<int> notesCopy = triggeredNotesOn;
	triggeredNotesOn.clear();
	return notesCopy;
}

std::vector<int> KinectFrameDifference::getTriggeredNotesOff() {
	std::vector<int> notesCopy = triggeredNotesOff;
	triggeredNotesOff.clear();
	return notesCopy;
}
void KinectFrameDifference::setPianoMode(bool _isPianoMode) {
	isPianoMode = _isPianoMode;
	cout << rectsAmount << ", " << rectsWidth << endl;
	if (isPianoMode) {
		createRectangles(rectsAmount, rectsWidth, rectsTop, rectsHeight);
	}
	if (!isPianoMode) {
		createZones(rectsAmount, rectsWidth, rectsTop, rectsHeight);
	}
}