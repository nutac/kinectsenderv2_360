#pragma once

#include "ofMain.h"
#include "ofxNDI.h"
//#include "ofxMidi.h"
//#define RECT_SPEED 5

class KinectFrameDifference {
public:
	KinectFrameDifference();

	void setup(int width, int height, string ndiSenderName);
	//void update(ofPixels_<unsigned short>& depthPixels, int _near, int _far, const int& alphaDecay, const float& minPixelsToTrigger, int _rectAlphaMin, int midiStep);
	void update(const ofPixels& depthPixels, bool showDifferencePixels, const int& alphaDecay, const float& minPixelsToTrigger, int _rectAlphaMin, int midiStep, int _sensibilidad);
	void updateRectsPosition();

	void draw(ofRectangle targetRectangle);
	void send();
	bool isRectTriggered(const ofRectangle& rect, const int& alpha, const ofPixels& differenceImage, const float& minChangedPixels);
	void createRectangles(int amount, int width, int top, int height);
	void createZones(int amount, int width, int top, int height);

	void setPianoMode(bool _isPianoMode);

	ofTexture processedTexture;
	ofTexture processedTexture2;

	//ofxMidiOut midiOut;
	std::vector<int> getTriggeredNotesOn();
	std::vector<int> getTriggeredNotesOff();

private:

	float w, h;
	
	int firstBoot = 60;

	ofPixels processedPixels;
	ofPixels previousPixels;
	ofPixels differencePixels;
	ofFbo fbo;

	std::vector<ofRectangle> rectangles;
	std::vector<int> rectAlpha;
	std::vector<bool> rectTriggered;

	int rectAlphaMin = 50;

	ofxNDIsender ndiSender;
	string senderName;

	bool showDifferencePixels;

	bool isPianoMode;

	std::vector<int> triggeredNotesOn;
	std::vector<int> triggeredNotesOff;

	int rectsAmount;
	float rectsWidth;
	float rectsTop;
	float rectsHeight;

	int sensibilidad;

	//const std::vector<int>& getTriggeredNotes() const;

	std::vector<ofVec2f> rectVelocities;

};
#pragma once
