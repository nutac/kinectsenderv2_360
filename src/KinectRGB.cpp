#include "KinectRGB.h"

KinectRGB::KinectRGB() {
}

void KinectRGB::setup(int _width, int _height, string ndiSenderName) {
	senderName = ndiSenderName;
	//fbo.allocate(width, height, GL_RGBA);

	rgbWidth = 1920;
	rgbHeight = 1920;

	fbo.allocate(rgbWidth, rgbHeight, GL_RGBA);


	ndiSender.SetReadback();
	ndiSender.SetAsync();
	ndiSender.CreateSender(senderName.c_str(), rgbWidth, rgbHeight);
	width = _width;
	height = _height*0.6;
}


void KinectRGB::draw(int x, int y, ofTexture& texture) {
	if (texture.isAllocated()) {
		fbo.begin();
		texture.draw(0, 0, rgbWidth, rgbHeight);
		fbo.end();
		fbo.draw(x, y, width, height);
	}
}

void KinectRGB::send() {
	ndiSender.SendImage(fbo);
}
