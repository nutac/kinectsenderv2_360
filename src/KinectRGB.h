#pragma once
#include "ofMain.h"
#include "ofxNDI.h"

class KinectRGB {
public:
    KinectRGB();

    void setup(int width, int height, string ndiSenderName);

    void draw(int x, int y, ofTexture& texture);
    void send();
    int width, height;

    int rgbWidth, rgbHeight;
private:
    ofFbo fbo;

    ofxNDIsender ndiSender;
    string senderName;
};
