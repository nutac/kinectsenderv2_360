#include "KinectThreshold.h"

KinectThreshold::KinectThreshold() {
}

void KinectThreshold::setup(int width, int height, string ndiSenderName) {
	processedPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
	processedTexture.allocate(processedPixels);

	senderName = ndiSenderName;
	fbo.allocate(width, height, GL_RGBA);
	ndiSender.SetReadback();
	ndiSender.SetAsync();
	ndiSender.CreateSender(senderName.c_str(), width, height);

	depthFbo.allocate(width, height, GL_RGBA);
}

//void KinectThreshold::update(ofPixels_<unsigned short>& depthPixels, int _near, int _far) {
void KinectThreshold::update(ofTexture& depthTexture, int _near, int _far) {
	//ofPixels_<unsigned short> depthPixels;
	//ofShortPixels depthPixels;

	//depthTexture.readToPixels(depthPixels);
	//depthTexture.readToPixels(depthPixels, GL_RED_INTEGER);

	depthFbo.begin();
	depthTexture.draw(0, 0);
	depthFbo.end();
	//ofPixels depthPixels;
	ofPixels_<unsigned short> depthPixels;

	depthFbo.readToPixels(depthPixels);

	int _near2 = _near * 100;
	int _far2 = _far * 100;
	int height = depthPixels.getHeight();
	int width = depthPixels.getWidth();
	//cout << "Depth value at (100,100): " << depthPixels.getColor(100, 100).getBrightness() << endl;

	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			unsigned short depthValue = depthPixels.getColor(x, y).getBrightness();
			//int depthValue = depthPixels.getColor(x, y).getBrightness();

			//if (depthValue > _near2 && depthValue < _far2) {
			if (depthValue > _far2 && depthValue < _near2) {
				
			//if (depthValue > _near2) {
			//if (depthValue < _far2) {

				int col = ofMap(depthValue, _far2, _near2, 50, 255);
				//int col = ofMap(depthValue, _near2, _far2, 50, 255);
				// 
				//int col = ofMap(depthValue, 0, 255, 50, 255);
				processedPixels.setColor(x, y, ofColor(col));
			}
			else {
				processedPixels.setColor(x, y, ofColor(0));
			}
		}
	}
	processedTexture.loadData(processedPixels);
	//processedTexture.loadData(depthPixels);

}

void KinectThreshold::draw(int x, int y) {
	fbo.begin();
	processedTexture.draw(0, 0);
	fbo.end();
	fbo.draw(x, y);
}

void KinectThreshold::send() {
	ndiSender.SendImage(fbo);
}
