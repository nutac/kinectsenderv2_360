#pragma once
#include "ofMain.h"
#include "ofxMidi.h"

class MidiManager {
public:
    MidiManager();
    ~MidiManager();

    void setup();
    void sendNoteOn(int channel, int pitch, int velocity);
    void sendNoteOff(int channel, int pitch);

private:
    ofxMidiOut midiOut;
};
