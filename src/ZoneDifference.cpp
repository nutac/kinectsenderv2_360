#include "ZoneDifference.h"

ZoneDifference::ZoneDifference() {
}

//HACER:
/*
Parametros q faltan:
-poder setear el rango de colores del threshold (degrade?)
-el color minimo de los rectangles
poder mover la camara del pointcloud

mandar midi
*/

void ZoneDifference::setup(int width, int height, string ndiSenderName) {
	//processedPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
	//previousPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
	//differencePixels.allocate(width, height, OF_IMAGE_GRAYSCALE);

	w = width;
	h = height;

	processedPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
	previousPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
	differencePixels.allocate(width, height, OF_IMAGE_GRAYSCALE);

	//previousPixels.set(0);
	processedTexture.allocate(processedPixels);
	processedTexture2.allocate(previousPixels);


	senderName = ndiSenderName;
	fbo.allocate(width, height, GL_RGBA);
	ndiSender.SetReadback();
	ndiSender.SetAsync();
	ndiSender.CreateSender(senderName.c_str(), width, height);

	createRectangles(10, 50);

	//int port = 0;
	//vector<string> portNames = midiOut.getOutPortList();
	//for (size_t i = 0; i < portNames.size(); i++) {
	//	cout << portNames[i] << endl;
	//	if (portNames[i].find("LoopBe") != string::npos) {
	//		port = i;
	//		break;
	//	}
	//}
	//midiOut.openPort(port);

}
void ZoneDifference::createRectangles(int amount, int width) {
	//cout << "createRectangles:" << endl;
	//cout << amount << ", " << width << endl;
	//if (width == 0) {
	//	cout << "width no puede ser 0!!!!" << endl;
	//	return;
	//}
	rectangles.clear();
	rectAlpha.clear();
	//int rectWidth = differencePixels.getWidth() / amount;
	int rectWidth = w / amount;

	for (int i = 0; i < amount; i++) {
		ofRectangle rect;
		rect.x = i * rectWidth;
		rect.y = 0;
		rect.width = rectWidth * float((float(width) / 100));
		//rect.height = differencePixels.getHeight();
		rect.height = h;

		rectangles.push_back(rect);

		rectAlpha.push_back(rectAlphaMin);

		rectTriggered.push_back(false);

	}
}

void ZoneDifference::update(const ofPixels& depthPixels, bool showDifferencePixels, const int& alphaDecay, const float& minPixelsToTrigger, int _rectAlphaMin, int midiStep) {
	//if (!depthTexture.bAllocated()) return;
	if (!depthPixels.isAllocated()) return;


	//ofPixels depthPixels;
	//depthPixels.allocate(depthTexture.getWidth(), depthTexture.getHeight(), OF_IMAGE_GRAYSCALE);

	//depthTexture.readToPixels(depthPixels);
	//int _near2 = _near * 1;
	//int _far2 = _far * 1;
	//int height = depthPixels.getHeight();
	//int width = depthPixels.getWidth();
	int height = h;
	int width = w;

	int step = 2;
	rectAlphaMin = _rectAlphaMin;

	//processedPixels.set(0); // Establece todos los p�xeles a negro


	//for (int y = 0; y < height-step; y += step) {
	//	for (int x = 0; x < width-step; x += step) {
	//		unsigned char depthValue = depthPixels.getColor(x, y).getBrightness();
	//		if (depthValue > _near2 && depthValue < _far2) {
	//			processedPixels.setColor(x, y, ofColor(255));
	//		}
	//		//else {
	//		//	processedPixels.setColor(x, y, ofColor(0));
	//		//}
	//	}
	//}
	const unsigned short DIFF_THRESHOLD = 10;
	differencePixels.set(0); // Establece todos los p�xeles a negro

	for (int y = 0; y < height - step; y += step) {
		for (int x = 0; x < width - step; x += step) {
			//unsigned short currentBrightness = processedPixels.getColor(x, y).getBrightness();
			//unsigned short prevBrightness = previousPixels.getColor(x, y).getBrightness();
			//unsigned char currentBrightness = processedPixels.getColor(x, y).getBrightness();
			unsigned char currentBrightness = depthPixels.getColor(x, y).getBrightness();

			unsigned char prevBrightness = previousPixels.getColor(x, y).getBrightness();

			if (abs(currentBrightness - prevBrightness) > DIFF_THRESHOLD) {
				differencePixels.setColor(x, y, ofColor(255));
			}
			//else {
			//	differencePixels.setColor(x, y, ofColor(0));
			//}
		}
	}

	for (int i = 0; i < rectAlpha.size(); i++) {
		rectAlpha[i] = rectAlpha[i] - alphaDecay;
		if (rectAlpha[i] <= rectAlphaMin) {
			rectAlpha[i] = rectAlphaMin;
			if (rectTriggered[i] == true) {
				rectTriggered[i] = false;
				//midiOut.sendNoteOff(1, i * midiStep);
				triggeredNotesOff.push_back(i * midiStep);
			}
		}
	}

	if (firstBoot <= 0) {
		/// TRIGGER
		for (int i = 0; i < rectangles.size(); i++) {
			if (isRectTriggered(rectangles[i], rectAlpha[i], differencePixels, minPixelsToTrigger)) {
				rectAlpha[i] = 255;
				rectTriggered[i] = true;
				int pitch = i * midiStep;
				//midiOut.sendNoteOn(1, pitch, 127);
				triggeredNotesOn.push_back(pitch);

			}
		}
	}
	else {
		//firstBoot = false;
		firstBoot--;
	}


	//for (int y = 0; y < height; y += step) {
	//	for (int x = 0; x < width; x += step) {
	//		//unsigned short depthValue = processedPixels.getColor(x, y).getBrightness();
	//		unsigned short depthValue = depthPixels.getColor(x, y).getBrightness();

	//		previousPixels.setColor(x, y, ofColor(depthValue));
	//	}
	//}
	previousPixels = depthPixels;
	processedTexture.loadData(differencePixels);
	//processedTexture2.loadData(previousPixels);


	fbo.begin();
	ofClear(0, 0, 0, 255);

	if (showDifferencePixels) {
		processedTexture.draw(0, 0);
	}

	for (int i = 0; i < rectangles.size(); i++) {
		ofSetColor(255, rectAlpha[i]);
		ofDrawRectangle(rectangles[i]);
	}

	fbo.end();

}

void ZoneDifference::draw(ofRectangle targetRectangle) {
	//if (!processedTexture.bAllocated()) return;
	if (!fbo.isAllocated()) return;


	fbo.draw(targetRectangle);
	//processedTexture2.draw(220, 430);
}

void ZoneDifference::send() {
	if (!fbo.isAllocated()) return;

	ndiSender.SendImage(fbo);
}

bool ZoneDifference::isRectTriggered(const ofRectangle& rect, const int& alpha, const ofPixels& differenceImage, const float& minChangedPixels) {
	int count = 0;
	bool res = false;
	int totalPixels = rect.getArea();
	int minPixels = totalPixels * float((float(minChangedPixels) / 100));
	if (alpha <= rectAlphaMin) {
		for (int y = rect.y; y < rect.y + rect.height; y++) {
			for (int x = rect.x; x < rect.x + rect.width; x++) {
				if (differenceImage.getColor(x, y).getBrightness() > 0) {
					count++;
					if (count > minPixels) {
						res = true;
						break;
					}
				}
			}
			if (res == true) break;
		}
	}
	return res;
}
std::vector<int> ZoneDifference::getTriggeredNotesOn() {
	std::vector<int> notesCopy = triggeredNotesOn;
	triggeredNotesOn.clear();
	return notesCopy;
}

std::vector<int> ZoneDifference::getTriggeredNotesOff() {
	std::vector<int> notesCopy = triggeredNotesOff;
	triggeredNotesOff.clear();
	return notesCopy;
}
