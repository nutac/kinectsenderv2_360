#pragma once
#pragma once

#include "ofMain.h"
#include "ofxNDI.h"
//#include "ofxMidi.h"

class ZoneDifference {
public:
	ZoneDifference();

	void setup(int width, int height, string ndiSenderName);
	//void update(ofPixels_<unsigned short>& depthPixels, int _near, int _far, const int& alphaDecay, const float& minPixelsToTrigger, int _rectAlphaMin, int midiStep);
	void update(const ofPixels& depthPixels, bool showDifferencePixels, const int& alphaDecay, const float& minPixelsToTrigger, int _rectAlphaMin, int midiStep);

	void draw(ofRectangle targetRectangle);
	void send();
	bool isRectTriggered(const ofRectangle& rect, const int& alpha, const ofPixels& differenceImage, const float& minChangedPixels);
	void createRectangles(int amount, int width);

	ofTexture processedTexture;
	ofTexture processedTexture2;

	//ofxMidiOut midiOut;
	std::vector<int> getTriggeredNotesOn();
	std::vector<int> getTriggeredNotesOff();

private:

	float w, h;

	int firstBoot = 60;

	ofPixels processedPixels;
	ofPixels previousPixels;
	ofPixels differencePixels;
	ofFbo fbo;

	std::vector<ofRectangle> rectangles;
	std::vector<int> rectAlpha;
	std::vector<bool> rectTriggered;

	int rectAlphaMin = 50;

	ofxNDIsender ndiSender;
	string senderName;

	bool showDifferencePixels;

	std::vector<int> triggeredNotesOn;
	std::vector<int> triggeredNotesOff;

	//const std::vector<int>& getTriggeredNotes() const;


};
#pragma once
