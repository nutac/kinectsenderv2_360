#include "ofApp.h"
#include "ofxGui.h"

//--------------------------------------------------------------
void ofApp::setup() {

	ofxKinectV2 tmp;
	std::vector <ofxKinectV2::KinectDeviceInfo> deviceList = tmp.getDeviceList();
	//allocate for this many devices
	kinects.resize(deviceList.size());
	texDepth.resize(kinects.size());
	texRGB.resize(kinects.size());
	//texRGBRegistered.resize(kinects.size());
	//texIR.resize(kinects.size());

	ofxKinectV2::Settings ksettings;
	ksettings.enableRGB = true;
	//ksettings.enableIR = true;
	ksettings.enableDepth = true;
	//ksettings.enableRGBRegistration = true;
	ksettings.config.MinDepth = 0.5;
	ksettings.config.MaxDepth = 8.0;

	for (int d = 0; d < kinects.size(); d++) {
		kinects[d] = std::make_shared<ofxKinectV2>();
		kinects[d]->open(deviceList[d].serial, ksettings);
		gui.add(kinects[d]->params);
	}

	setupGui();

	depthWidth = 512;
	depthHeight = 428;
	rgbWidth = 1920;
	rgbHeight = 1080;

	//kinectThreshold.setup(depthWidth, depthHeight, "KinectThreshold");
	//kinectPointCloud.setup(depthWidth, depthHeight, "kinectPointCLoud");

	//kinectParticleSystem.setup(depthWidth, depthHeight, "kinectParticleSystem", totalParticles);

	//kinectFrameDifference.setup(depthWidth, depthHeight, "KinectFrameDifference");
	//kinectFrameDifference.createRectangles(totalRectangles, widthRectangles);

	//fluidFlow.setup(depthWidth, depthHeight, "fluidFlow");

	//kinectRGB.setup(depthWidth, depthHeight, "kinectRGB");


	depthSender.resize(kinects.size());
	rgbSender.resize(kinects.size());
	fluidFlow.resize(kinects.size());
	kinectFrameDifference.resize(kinects.size());
	//zoneDifference.resize(kinects.size());



	for (int i = 0; i < kinects.size(); i++) {
		depthSender[i] = std::make_shared<ofxNDIsender>();
		depthSender[i]->SetReadback();
		depthSender[i]->SetAsync();
		string senderName = "depthSender" + ofToString(i);
		depthSender[i]->CreateSender(senderName.c_str(), depthWidth, depthHeight);

		rgbSender[i] = std::make_shared<ofxNDIsender>();
		rgbSender[i]->SetReadback();
		rgbSender[i]->SetAsync();
		senderName = "RGBSender" + ofToString(i);
		rgbSender[i]->CreateSender(senderName.c_str(), rgbWidth, rgbHeight);

		fluidFlow[i] = std::make_shared<FluidFlow>();
		//fluidFlow[i]->setup(depthWidth, depthHeight, "fluidFlow" + ofToString(i));
		fluidFlow[i]->setup(rgbWidth, rgbHeight, "fluidFlow" + ofToString(i));
		//if (!ofFile("settings2" + ofToString(i) + ".xml")) {
		//	fluidFlow[i]->gui.saveToFile("settings2" + ofToString(i) + ".xml");
		//}
		//fluidFlow[i]->gui.loadFromFile("settings2" + ofToString(i) + ".xml");

		kinectFrameDifference[i] = std::make_shared<KinectFrameDifference>();
		kinectFrameDifference[i]->setup(depthWidth, depthHeight, "KinectFrameDifference" + ofToString(i));
		kinectFrameDifference[i]->createRectangles(totalRectangles, widthRectangles, topRectangles, heightRectangles);

		//zoneDifference[i] = std::make_shared<ZoneDifference>();
		//zoneDifference[i]->setup(depthWidth, depthHeight, "ZoneDifference" + ofToString(i));
		//zoneDifference[i]->createRectangles(totalRectangles, widthRectangles);

	}

	//totalRectangles.addListener(this, &ofApp::totalRectanglesChanged);
	//widthRectangles.addListener(this, &ofApp::widthRectanglesChanged);
	

	totalRectangles.addListener(this, &ofApp::setFrameDifferenceRectangles);
	widthRectangles.addListener(this, &ofApp::setFrameDifferenceRectangles);
	topRectangles.addListener(this, &ofApp::setFrameDifferenceRectangles);
	heightRectangles.addListener(this, &ofApp::setFrameDifferenceRectangles);

	pianoMode.addListener(this, &ofApp::pianoModeChanged);

	midiManager.setup();

}
void ofApp::setupGui() {
	//totalParticles.addListener(this, &ofApp::totalParticlesChanged);

	saveButton.addListener(this, &ofApp::saveButtonPressed);

	thresholdParams.setName("Kinect Depth");
	//thresholdParams.setName("THRESHOLD");
	minDistance.addListener(this, &ofApp::setKinectThresholdDistance);
	maxDistance.addListener(this, &ofApp::setKinectThresholdDistance);
	thresholdParams.add(showKinectThreshold.set("ShowKinectDepth", true));
	thresholdParams.add(sendKinectThreshold.set("SendKinectDepth", true));

	//thresholdParams.add(minDistance.set("minDistance", 0.5, 0.4, 7.0));
	//thresholdParams.add(maxDistance.set("maxDistance", 8.0, 0.5, 8.0));
	thresholdParams.add(minDistance.set("minDistance", 450, 0, 4000));
	thresholdParams.add(maxDistance.set("maxDistance", 3000, 0, 6000));
	//thresholdParams.add(_near.set("near", 50, 0, 700));
	//thresholdParams.add(_far.set("far", 200, 0, 700));

	kinectRGBParams.setName("kinect RGB");
	kinectRGBParams.add(showKinectRGB.set("showKinectRGB", true));
	kinectRGBParams.add(sendKinectRGB.set("sendKinectRGB", true));


	fluidFlowParams.setName("Fluid Flow");
	fluidFlowParams.add(showFluidFlow.set("ShowFluidFlow", true));
	fluidFlowParams.add(sendFluidFlow.set("SendFluidFlow", true));
	fluidFlowParams.add(showFluidFlowDepth.set("showFluidFlowDepth", false));
	fluidFlowParams.add(showFluidParameters.set("ShowFluidParameters", false));

	frameDifferenceParams.setName("Frame Difference");
	frameDifferenceParams.add(showFrameDifference.set("ShowFrameDifference", true));
	frameDifferenceParams.add(sendFrameDifference.set("sendFrameDifference", true));
	frameDifferenceParams.add(showDifferencePixels.set("showDifferencePixels", true));
	frameDifferenceParams.add(sensibilidad.set("Sensibilidad", 4, 1, 180));
	frameDifferenceParams.add(alphaDecay.set("Alpha Decay", 4, 1, 100));
	//frameDifferenceParams.add(minPixelsToTrigger.set("minPixelsToTrigger", 50, 10, 500));
	frameDifferenceParams.add(minPixelsToTrigger.set("PctPixelsToTrigger", 5, 0.1, 10));
	frameDifferenceParams.add(baseAlpha.set("baseAlpha", 50, 0, 200));
	frameDifferenceParams.add(midiStep.set("midiStep", 2, 1, 12));
	frameDifferenceParams.add(pianoMode.set("setPianoMode", true));

	pianoModeParams.setName("Piano Mode");
	pianoModeParams.add(totalRectangles.set("Total Rectangles", 10, 5, 20));
	pianoModeParams.add(widthRectangles.set("Width Rectangles", 50, 10, 100));
	pianoModeParams.add(topRectangles.set("Top Rectangles", 0, 0, 100));
	pianoModeParams.add(heightRectangles.set("Height Rectangles", 100, 10, 100));

	frameDifferenceParams.add(pianoModeParams);

	pointCloudParams.setName("Kinect Point Cloud");
	pointCloudParams.add(showKinectPointCloud.set("ShowKinectPointCloud", true));
	pointCloudParams.add(depthMax.set("Depth Max", 100, 0, 500));
	pointCloudParams.add(step.set("Step", 4, 1, 10));
	pointCloudParams.add(pointSize.set("Point Size", 2, 1, 10));

	particleSystemParams.setName("Particle System");
	particleSystemParams.add(showParticleSystem.set("ShowParticleSystem", true));
	particleSystemParams.add(distance.set("Distance", 50, 0, 100));
	particleSystemParams.add(alpha.set("Alpha", 80, 0, 255));
	particleSystemParams.add(showDots.set("Show dots", true));
	particleSystemParams.add(totalParticles.set("Total Particles", 500, 100, 3000));


	gui.setup();

	//color1a = ofColor(0, 24, 143);
	color1a = ofColor(36, 160, 255);
	color1b = color1a.getLerped(ofColor::black, 0.3);
	//color2a = ofColor(0, 178, 148);
	color2a = ofColor(206, 24, 250);
	color2b = color2a.getLerped(ofColor::black, 0.3);
	//color3a = ofColor(236, 0, 140);
	color3a = ofColor(255, 62, 101);
	color3b = color3a.getLerped(ofColor::black, 0.3);
	//color4a = ofColor(0, 178, 148);
	color4a = ofColor(0, 205, 190);
	color4b = color4a.getLerped(ofColor::black, 0.3);

	gui.setHeaderBackgroundColor(color1b);
	gui.setBorderColor(ofColor(50));
	//gui.setDefaultBackgroundColor(lightBaseColor);
	//gui.setBackgroundColor(baseColor);

	gui.add(saveButton.setup("Guardar"));


	gui.setDefaultHeaderBackgroundColor(color1a);
	gui.setDefaultFillColor(color1b);
	gui.add(thresholdParams);

	gui.setDefaultHeaderBackgroundColor(color2a);
	gui.setDefaultFillColor(color2b);
	gui.add(kinectRGBParams);

	gui.setDefaultHeaderBackgroundColor(color1a);
	gui.setDefaultFillColor(color1b);
	gui.add(fluidFlowParams);

	gui.setDefaultHeaderBackgroundColor(color4a);
	gui.setDefaultFillColor(color4b);
	gui.add(frameDifferenceParams);

	//gui.setDefaultHeaderBackgroundColor(color2a);
	//gui.setDefaultFillColor(color2b);
	//gui.add(pointCloudParams);

	//gui.setDefaultHeaderBackgroundColor(color3a);
	//gui.setDefaultFillColor(color3b);
	//gui.add(particleSystemParams);

	gui.loadFromFile("settings.xml");
}
//--------------------------------------------------------------
void ofApp::setKinectThresholdDistance(float& value) {
	for (int i = 0; i < kinects.size(); i++) {
		kinects[i]->minDistance = minDistance;
		kinects[i]->maxDistance = maxDistance;
	}

}
void ofApp::update() {
	//kinect.update();
	float fps = ofGetFrameRate();
	ofSetWindowTitle("FPS: " + ofToString(fps));

	for (int d = 0; d < kinects.size(); d++)
	{
		kinects[d]->update();

		if (kinects[d]->isFrameNew())
		{
			//if (kinects[d]->getRegisteredPixels().getWidth() > 10) texRGBRegistered[d].loadData(kinects[d]->getRegisteredPixels());
			//if (kinects[d]->isIREnabled()) texIR[d].loadData(kinects[d]->getIRPixels());

			if (kinects[d]->isRGBEnabled()) {
				texRGB[d].loadData(kinects[d]->getPixels());
				if (sendKinectRGB)
					rgbSender[d]->SendImage(texRGB[d]);

			}
			if (kinects[d]->isDepthEnabled()) {
				texDepth[d].loadData(kinects[d]->getDepthPixels());
				if (sendKinectThreshold)
					depthSender[d]->SendImage(texDepth[d]);
			};

			if (showFrameDifference || sendFrameDifference) {
				kinectFrameDifference[d]->update(kinects[d]->getDepthPixels(), showDifferencePixels, alphaDecay, minPixelsToTrigger, baseAlpha, midiStep, sensibilidad);

				for (int pitch : kinectFrameDifference[d]->getTriggeredNotesOn()) {
					midiManager.sendNoteOn(1 + d, pitch, 127);
				}

				for (int pitch : kinectFrameDifference[d]->getTriggeredNotesOff()) {
					midiManager.sendNoteOff(1 + d, pitch);
				}
			}

		}

		if (showFluidFlow || sendFluidFlow) {
			if (texRGB[d].bAllocated())
				//fluidFlow[d]->update(texDepth[d]);
				fluidFlow[d]->update(texRGB[d], showFluidFlowDepth);
		}


	}


	//if (showKinectPointCloud) {
	//	kinectPointCloud.update(depthPixels, _near, _far, depthMax, step, pointSize);
	//}
	//if (showParticleSystem) {
	//	kinectParticleSystem.update(distance);
	//}

}

//--------------------------------------------------------------
void ofApp::draw() {

	ofBackground(50);

	int left = 220;
	int top2 = 435;
	//if (showKinectThreshold) {
	//	kinectThreshold.draw(left, 0);
	//	kinectThreshold.send();
	//}

	for (int i = 0; i < kinects.size(); i++) {
		if (showKinectThreshold) {
			drawTextureInRectangle("Kinect " + ofToString(i) + " - Depth", texDepth[i], i, 0);
		}

		if (showKinectRGB) {
			drawTextureInRectangle("Kinect " + ofToString(i) + " - RGB", texRGB[i], i + 2, 0);
		}

		if (showFluidFlow) {
			fluidFlow[i]->draw(getTargetRectangle(i, 1, true), showFluidFlowDepth, showFluidParameters);
		}
		if (sendFluidFlow) {
			fluidFlow[i]->send();
		}

		if (showFrameDifference) {
			kinectFrameDifference[i]->draw(getTargetRectangle(i + 2, 1));
		}
		if (sendFrameDifference) {
			kinectFrameDifference[i]->send();
		}
	}

	//if (showKinectPointCloud) {
	//	kinectPointCloud.draw(left + (depthWidth + 10) * 1, 0);
	//	kinectPointCloud.send();
	//}
	//if (showParticleSystem) {
	//	kinectParticleSystem.draw(left + (depthWidth + 10) * 2, 0, kinectThreshold.processedTexture, alpha, showDots);
	//}

	gui.draw();
}
//void ofApp::totalParticlesChanged(int& totalParticles) {
	//kinectParticleSystem.createParticles(totalParticles);
//}
//void ofApp::totalRectanglesChanged(int& value) {
//	for (int i = 0; i < kinects.size(); i++) {
//		kinectFrameDifference[i]->createRectangles(totalRectangles, widthRectangles);
//	}
//}
//void ofApp::widthRectanglesChanged(int& value) {
//	for (int i = 0; i < kinects.size(); i++) {
//		kinectFrameDifference[i]->createRectangles(totalRectangles, widthRectangles);
//	}
//}
void ofApp::setFrameDifferenceRectangles(int& value) {
	for (int i = 0; i < kinects.size(); i++) {
		kinectFrameDifference[i]->createRectangles(totalRectangles, widthRectangles, topRectangles, heightRectangles);
	}
}

void ofApp::pianoModeChanged(bool& pianoMode) {
	for (int i = 0; i < kinects.size(); i++) {
		kinectFrameDifference[i]->setPianoMode(pianoMode);
	}
}

void ofApp::saveButtonPressed() {
	gui.saveToFile("settings.xml");
	//fluidFlow.gui.saveToFile("settings2.xml");
	for (int d = 0; d < kinects.size(); d++) {
		fluidFlow[d]->gui.saveToFile("settings2" + ofToString(d) + ".xml");

	}
	fluidFlow[0]->gui.saveToFile("settings2.xml");

}


//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	if (key == 's') {
		gui.saveToFile("settings.xml");
		//fluidFlow.gui.saveToFile("settings2.xml");
		for (int d = 0; d < kinects.size(); d++) {
			fluidFlow[d]->gui.saveToFile("settings2" + ofToString(d) + ".xml");
		}
		fluidFlow[0]->gui.saveToFile("settings2.xml");

	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}

void ofApp::drawTextureInRectangle(const std::string& title,
	const ofTexture& tex,
	int row,
	int column) {


	ofRectangle targetRectangle = getTargetRectangle(row, column);
	ofNoFill();
	ofSetColor(ofColor::gray);
	ofDrawRectangle(targetRectangle);

	ofFill();
	ofSetColor(255);
	if (tex.isAllocated())
	{
		ofRectangle textureRectangle(0, 0, tex.getWidth(), tex.getHeight());

		// Scale the texture rectangle to its new location and size.
		textureRectangle.scaleTo(targetRectangle);
		tex.draw(textureRectangle);
	}
	else
	{
		ofDrawBitmapStringHighlight("...",
			targetRectangle.getCenter().x,
			targetRectangle.getCenter().y);
	}

	ofDrawBitmapStringHighlight(title,
		targetRectangle.getPosition() + glm::vec3(14, 20, 0));

}

ofRectangle ofApp::getTargetRectangle(int row, int column, bool isSixteen9) {
	float guiWidth = 225;
	float displayWidth = 512 * 0.75;
	float displayHeight = 428 * 0.75;
	float displayHeight2 = displayHeight;
	if (isSixteen9) displayHeight2 = 428 * 0.55;

	return ofRectangle(guiWidth + row * displayWidth,
		column * displayHeight,
		displayWidth,
		displayHeight2);
}