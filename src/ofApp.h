#pragma once

//#include "ofxKinectForWindows2.h"
#include "ofxKinectV2.h"

#include "ofMain.h"
#include "ofxNDI.h" 
#include "ofxGui.h"
#include "KinectThreshold.h"
//#include "KinectPointCloud.h"
//#include "KinectParticleSystem.h"
#include "KinectFrameDifference.h"
#include "MidiManager.h"
#include "FluidFlow.h"
#include "KinectRGB.h"
//#include "ZoneDifference.h"


class ofApp : public ofBaseApp {

public:
	void setup();
	void setupGui();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	//void totalParticlesChanged(int& totalParticles);
	//void totalRectanglesChanged(int& totalRectangles);
	//void widthRectanglesChanged(int& widthRectangles);
	void setFrameDifferenceRectangles(int& value);
	void pianoModeChanged(bool& pianoMode);
	void saveButtonPressed();

	void drawTextureInRectangle(const std::string& title,
		const ofTexture& tex,
		int row,
		int column);
	ofRectangle getTargetRectangle(int row, int column, bool isSixteen9 = false);



	void setKinectThresholdDistance(float& value);

	std::vector<std::shared_ptr<ofxKinectV2>> kinects;
	std::vector<ofTexture> texRGB;
	//std::vector<ofTexture> texRGBRegistered;
	//std::vector<ofTexture> texIR;
	std::vector<ofTexture> texDepth;

	//ofxKFW2::Device kinect;
	//KinectThreshold kinectThreshold;
	//KinectPointCloud kinectPointCloud;
	//KinectParticleSystem kinectParticleSystem;


	//FluidFlow fluidFlow;
	std::vector<std::shared_ptr<FluidFlow>> fluidFlow;
	std::vector<std::shared_ptr<KinectFrameDifference>> kinectFrameDifference;
	//std::vector<std::shared_ptr<ZoneDifference>> zoneDifference;

	// 
	MidiManager midiManager;
	//KinectRGB kinectRGB;

	int depthWidth;
	int depthHeight;
	int rgbWidth, rgbHeight;


	// GUI
	ofxPanel gui;

	ofParameterGroup thresholdParams;
	ofParameter<float> minDistance;
	ofParameter<float> maxDistance;

	//ofxNDIsender depthSender;
	std::vector<std::shared_ptr<ofxNDIsender>> depthSender;
	std::vector<std::shared_ptr<ofxNDIsender>> rgbSender;

	//std::vector<ofxNDIsender> depthSender;


	//ofParameter<glm::vec2> threshold;
	//ofParameterGroup thresholdParams;
	ofParameter<bool> showKinectThreshold;
	ofParameter<bool> sendKinectThreshold;
	ofParameter<int> _near;
	ofParameter<int> _far;

	ofParameterGroup kinectRGBParams;
	ofParameter<bool> showKinectRGB;
	ofParameter<bool> sendKinectRGB;

	ofParameterGroup fluidFlowParams;
	ofParameter<bool> showFluidFlow;
	ofParameter<bool> sendFluidFlow;
	ofParameter<bool> showFluidFlowDepth;
	ofParameter<bool> showFluidParameters;
	ofParameter<bool> showDepth;

	/// frameDifference
	ofParameterGroup frameDifferenceParams;
	ofParameter<bool> showFrameDifference;
	ofParameter<bool> sendFrameDifference;
	ofParameter<bool> showDifferencePixels;
	ofParameter<int> sensibilidad;
	ofParameter<int> alphaDecay;
	ofParameter<float> minPixelsToTrigger;
	ofParameter<int> baseAlpha;
	ofParameter<int> midiStep;
	ofParameter<bool> pianoMode;

	ofParameterGroup pianoModeParams;
	ofParameter<int> totalRectangles;
	ofParameter<int> widthRectangles;
	ofParameter<int> topRectangles;
	ofParameter<int> heightRectangles;





	///
	ofParameterGroup pointCloudParams;
	ofParameter<bool> showKinectPointCloud;
	ofParameter<int> depthMax;
	ofParameter<int> step;
	ofParameter<float> pointSize;
	//ofParameter<bool> showMesh;

	///
	ofParameterGroup particleSystemParams;
	ofParameter<bool> showParticleSystem;
	ofParameter<int> distance;
	ofParameter<int> alpha;
	ofParameter<bool> showDots;
	ofParameter<int> totalParticles;








	ofxButton saveButton;

	ofColor color1a;
	ofColor color1b;
	ofColor color2a;
	ofColor color2b;
	ofColor color3a;
	ofColor color3b;
	ofColor color4a;
	ofColor color4b;



	//ofVideoGrabber webcam;
	//ofFbo fboWebcam;
	//ofxNDIsender ndiWebcam;
	//int webcamWidth;
	//int webcamHeight;

};
